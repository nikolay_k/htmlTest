/*
 * Author: N.Kovalenko
 * StartDate: 27.11/2017
 * EndDate: 28.11/2017
 */


'use strict';

var Company = window.Company || {};


Company = {

    init: function () {

        this.newsModuleOnLoad();
        this.filmstripSlider('[data-module="filmstripSlider"]');
    },

    /*
    * [data-module="filmstripSlider"]
    * Public Function To Slide Filmstrip - [data-module="filmstripSlider"]
    * */

    filmstripSlider: function (carouselParent) {
        // Default Settings
        var settings = {
            visible: 15,
            rotateBy: 1,
            speed: 1000,
            auto: true,
            margin: 10
        };

        var $this = $(carouselParent);
        var $ourCarousel = $this.children(':first');
        var itemWidth = $ourCarousel.children().outerWidth() + settings.margin;
        var itemHeight = $ourCarousel.children().outerHeight() + settings.margin;
        var itemsTotal = $ourCarousel.children().length;
        var isCarouselRun = false; //stop carousel
        var intID = null;
        var toSlide = true; // for stop on Hover

        var size = itemWidth;

        //basic css for slider wrapper
        $this.css({
            'position': 'relative',
            'overflow': 'hidden',
            'width': settings.visible * size + 'px',
            'height': itemHeight - settings.margin
        });

        //basic css for slider parent
        $ourCarousel.css({
            'position': 'relative',
            'width': 9999 + 'px',
            'top': 0,
            'left': 0
        });


        /*
        * Privet Function to Slide Logic
        */
        function slide(dir) {

            var direction = -1;
            var Indent = 0;

            if (!isCarouselRun) {

                isCarouselRun = true;

                if (intID) {
                    window.clearInterval(intID); // clearing interval
                }

                /* Clonning Logic */
                if (!dir) {
                    $ourCarousel.children(':last').after($ourCarousel.children().slice(0, settings.rotateBy).clone(true));
                } else {
                    $ourCarousel.children(':first').before($ourCarousel.children().slice(itemsTotal - settings.rotateBy, itemsTotal).clone(true));
                    $ourCarousel.css('left', -size * settings.rotateBy + 'px');
                }

                /* Moving Logic */

                Indent = parseInt($ourCarousel.css('left')) + (size * settings.rotateBy * direction);

                var animate_data = {'left': Indent};

                // starting animation
                $ourCarousel.animate(animate_data, {
                    easing: 'linear', queue: true, duration: settings.speed, complete: function () {

                        $ourCarousel.children().slice(0, settings.rotateBy).remove();
                        $ourCarousel.css('left', 0);

                        if (settings.auto) {

                            intID = window.setInterval(function () {
                                if (toSlide) {
                                    slide(settings.dirAutoSlide);
                                }
                            }, settings.auto);
                        }

                        isCarouselRun = false;
                    }
                });


            }

            return false;
        }

        if (settings.auto) {

            intID = window.setInterval(function () {
                if (toSlide) {
                    slide(settings.dirAutoSlide);
                }
            }, settings.auto);

            $ourCarousel.on('mouseenter', function (e) {
                e.preventDefault();
                e.stopPropagation();
                toSlide = false;
                isCarouselRun = true;
            });

            $ourCarousel.on('mouseleave', function (e) {
                e.preventDefault();
                e.stopPropagation();
                toSlide = true;
                isCarouselRun = false;
            });

        }

    },

    /*
     * data-module="newsModuleOnLoad"
     * Public Function For Add News TO data-module="newsModuleOnLoad"
     * */

    newsModuleOnLoad: function () {
        var url = "api/get_news/news.json",
            that = this ,
            errorText = "Sorry, can`t to load news, check your server settings or url";

        /* Private Func for Trim Text */
        function newsModuleTrim(content) {
            var showChar = 450;
            var ellipsestext = "...";

            if (content.length > showChar) {
                var newContent = content.substr(0, showChar);
                var trimContent = content.substr(showChar, content.length - showChar);
                var html = newContent + '<span class="moreDots">' + ellipsestext + '</span><span class="moreText"><span>' + trimContent + '</span></span>';

            }

            return html;
        }

        /* Private Func for Append News */
        function appendNews(response) {
            $('[data-module="newsModuleOnLoad"]').append(
                '<div class="newsModule__item">' +
                    '<div class="newsModule__item-header">' +
                        '<h3>' + response.title + '</h3>' +
                        '<div class="date">'
                        + response.date +
                        '</div>' +
                    '</div>' +
                    '<div class="newsModule__item-content" data-module="newsModuleTrim">'
                        + newsModuleTrim(response.content) +
                    '</div>' +
                    '<button type="button" data-module="newsModuleShowMore">more ></button>' +
                '</div>'
            );

            return true;
        }

        /* Private Func for Ajax Request To Get News API.
         * URL - api/get_news/news.json
         * */

        $.ajax({
            type: "GET",
            dataType: "JSON",
            url: url,
            success: function (response) {

                if (response.succses === true){

                    /* Add News */
                    for (var i = 0; i < response.news.length; i++) {
                        appendNews(response.news[i]);
                    }

                    /* Init More Clicker */
                    that.newsModuleShowMore();

                    /* Set Status - News Added*/
                    $('[data-module="newsModuleOnLoad"]').attr('data-moduleLoadStatus', 'true');
                }

                return true;
            },
            error: function () {
                $('[data-module="newsModuleOnLoad"]').append('<p>' + errorText + '</p>');
                $('[data-module="newsModuleOnLoad"]').attr('data-moduleLoadStatus', 'true');
            }
        });
    },

    /*
    * data-module='newsModuleShowMore'
    * Public Function for Show More news In Module data-module="newsModuleOnLoad, when click More BTN"
    * */

    newsModuleShowMore: function () {
        var lesstext = "hide ", moretext = "more >";
        $("[data-module='newsModuleShowMore']").on('click', function () {
            if ($(this).hasClass("less")) {
                $(this).removeClass("less");
                $(this).html(moretext);
            } else {
                $(this).addClass("less");
                $(this).html(lesstext);
            }
            $(this).closest(".newsModule__item").find('.moreText span').toggle();
            $(this).closest(".newsModule__item").find('.moreDots').toggle();

            return false;
        });
    }


};

/*
* Init Object
* */

$(document).ready(function () {
    Company.init();
});