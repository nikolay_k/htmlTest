var gulp = require('gulp'),
    sass = require('gulp-sass'),
    cache = require('gulp-cache'),
    del = require('del'),
    autoprefixer = require('gulp-autoprefixer'),
    rigger = require('gulp-rigger'),
    browserSync = require('browser-sync');


gulp.task('html', function() {
    gulp.src('app/pages/*.html')
        .pipe(rigger())
        .pipe(gulp.dest('./'))
        .pipe(browserSync.reload({ stream: true }));
});

gulp.task('sass', function() {
    return gulp.src('app/scss/**/*.+(scss|sass)')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true }))
        .pipe(gulp.dest('./dist/css'))
        .pipe(browserSync.reload({ stream: true }))
});


gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: './'
        },
        notify: false
    });
});


gulp.task('clean', function() {
    return del.sync('dist');
});

gulp.task('clear', function() {
    return cache.clearAll();
});


gulp.task('watch', ['browser-sync', 'html', 'sass'], function() {
    gulp.watch('app/scss/**/*.+(scss|sass)', ['sass']);
    gulp.watch('app/pages/*.html', ['html']);
    gulp.watch('app/pages/common/*.html', ['html']);
    gulp.watch('app/pages/*/*.html', ['html']);
    gulp.watch('app/js/**/*.js', browserSync.reload);
});


gulp.task('default', ['watch']);